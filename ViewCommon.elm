module ViewCommon exposing (..)

import Prelude exposing (..)
import Helper exposing (..)
import Models exposing (..)
import Html.Attributes exposing (class, type_, href, class, style, classList, value, max, id, placeholder, attribute, value, src)
import Html.Events exposing (onClick, on, onInput)
import Html exposing (Html, button, div, text, p, a, section, h1, h2, table, thead, tr, th, tfoot, td, tbody, i, span, progress, form, input, ul, li, label, input, figure, img, br)


controllerStyle : Html.Attribute a
controllerStyle = style [ ("position", "fixed")
                        , ("bottom", "20px")
                        , ("width", "80%")
                        , ("right", "10%")
                        , ("margin-left", "10%") ]

controlStyle : Html.Attribute a
controlStyle =  style [ ("margin-right", "5px") ] 
