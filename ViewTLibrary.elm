module ViewTLibrary exposing (viewTLibrary)

import Config exposing (..)
import ViewCommon exposing (..)
import List exposing (..)
import Debug exposing (log)
import Strings exposing (..)
import Prelude exposing (..)
import Helper exposing (..)
import Models exposing (..)
import Html.Attributes exposing (class, type_, href, class, style, classList, value, max, id, placeholder, attribute, value, src)
import Html.Events exposing (onClick, on, onInput)
import Html exposing (Html, button, div, text, p, a, section, h1, h2, table, thead, tr, th, tfoot, td, tbody, i, span, progress, form, input, ul, li, label, input, figure, img, br, nav)
import Maybe
import Html.Events.Extra exposing (onEnter)

viewTLibrary : Model -> Html Msg
viewTLibrary model =
    [ div [ class "columns" ]
      [ div [ class "column is-12 is-mobile" ] 
        [ viewForm model
        ]
      ]
    ,
      div [ class "columns" ]
        [ div [ class "column is-12 is-mobile" ]
            [ musicTable model
            ]
        ]
    , div [ class "columns" ]
        [ div [ class "column is-12 is-mobile"] 
            [ viewPage model 
            ]
        ]
    , hGap 70
    ] |> divHolderWithAttr [ onEnter DoLibrarySearch ]

viewForm : Model -> Html Msg
viewForm model =
    [ div [ class "field has-addons", flexStyle ]
        [ p [ class "control has-icons-left" ]
            [ input 
                [ classList 
                    [ ("input", True)
                    ]
                , type_ "text"
                , placeholder "search" 
                , value model.librarySearchString
                , onInput LibrarySearchInput
                ] 
                []
            , smallLeftIcon
            ]
        , p [ class "control" ]
            [ a [ class "button is-primary", onEnter DoLibrarySearch ] 
                [ text "Search" ]
            ]
        , p [ class "control" ]
            [ a [ class "button is-link", onClick LibrarySearchFormClear ] 
                [ text "Clear" ]
            ] 
        ]
    ] |> divHolder

musicTable : Model -> Html Msg
musicTable model =
    let ths = map (\t -> th [] [ text t ]) $ sTableHeads
        trs = map (\{pos, name, len, origin} -> tr []
                    [ td [] [ text $ toString (pos+1) ]
                    , td [] [ text name ]
                    , td [] [ text $ formatSeconds len ]
                    , td [] $ [ controls model name ]
                    ]) <| pageItems model.librarySearchCurrPage
                       <| case model.librarySearchResult of
                            Just l -> l
                            Nothing -> model.library
    in
    table [ class "table" ]
    [ thead [] 
      [ tr [] 
        ths  
      ]
    , tfoot []
      [ tr [] 
        ths 
      ]
    , tbody []
        trs 
    ]

pageItems : Int -> List a -> List a
pageItems currPage =
    let n = currPage - 1
        d = round <| toFloat n * librarySongPerPage
    in
        take (round librarySongPerPage) << drop d 

controls : Model -> String -> Html Msg
controls model name = 
    let controlItem = map (String.append "fa fa-") [ "play", "arrow-right", "fast-forward" ] 
        btnAttr =  classList <| (map2 (,) ["button", "is-small", "is-primary"] (repeat 3 True))
        controlAction = [ QueueSongAtNextAndPlay, QueueSongAtNext, QueueSongAtEnd ]
    in 
    map2 (\item a_ ->
        [ a [btnAttr, onClick $ a_ name , controlStyle ]
          [ span [ class "icon is-small" ]
            [ i [ class item ] [] 
            ]
          ]
        ])
        controlItem controlAction
    |> concat |> divHolder

viewPage : Model -> Html Msg
viewPage model =
    let pfn pl = ceiling <| toFloat (length pl) / librarySongPerPage
        pagesN = case model.librarySearchResult of
                    Just list -> pfn list
                    Nothing -> pfn model.library
    in
        mainLi model.librarySearchCurrPage pagesN

mainLi : Int -> Int -> Html Msg
mainLi currPage totalPages =
    let
        libraryPagination_ = 2 * libraryPagination + 1
        hasLeftEllipsis = currPage >= libraryPagination_
        hasRightEllipsis = (totalPages - currPage) > (libraryPagination + 1)
        left = currPage - libraryPagination
        right = currPage + libraryPagination
        left_ = if left <= 1 then 2 else left
        tpm = totalPages - 1
        right_ = if right > tpm then tpm else right
    in
        nav [ class "pagination is-centered" ]
        [ a [ class "pagination-previous"
            , displayAttr <| currPage /= 1 
            , onClick <| LibraryPageChange (currPage - 1) ]
          [ text "Previous" ]
        , a [ class "pagination-next"
            , displayAttr <| currPage /= totalPages 
            , onClick <| LibraryPageChange (currPage + 1) ]
          [ text "Next" ]
        , ul [ class "pagination-list" ]
            (
            [ Just << linkLi 1 <| currPage == 1
            , if hasLeftEllipsis then Just elipLi else Nothing
            ]
            ++
            rangeLi left_ right_ currPage
            ++
            [ if hasRightEllipsis then Just elipLi else Nothing
            , Just << linkLi totalPages <| currPage == totalPages
            ]
            |> catMaybes
            )
        ]

rangeLi : Int -> Int -> Int -> List (Maybe (Html Msg))
rangeLi start end curr =
    let range_ = range start end
        boolL = map (\x -> x == curr) range_
    in
        map2 (linkLi_) range_ boolL 

linkLi_ : Int -> Bool -> Maybe (Html Msg)
linkLi_ a b = Just <| linkLi a b


linkLi : Int -> Bool -> Html Msg
linkLi s isHighlight =
    li []
    [ a [ classList <| 
          [ ("pagination-link", True)
          , ("is-current", isHighlight)
          ]
        , onClick <| LibraryPageChange s
        ]
      [ text <| toString s ]
    ]

elipLi : Html a
elipLi =
    li []
    [ span [ class "pagination-ellipsis" ]
      [ text "..." ]
    ]
