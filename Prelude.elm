module Prelude exposing (..)

import Debug exposing (log)

($) = (<|)

get n xs = List.head (List.drop n xs)

safeGet : Int -> List a -> List a
safeGet n =
    List.take 1 << List.drop n

safeGetList : List Int -> List a -> List a
safeGetList ns l =
    List.foldl (\i a ->
        safeGet i l ++ a) [] (log "NS = " ns)

peelMaybe : Maybe a -> a -> a
peelMaybe m d =
    case m of 
        Just s -> s
        _ -> d

peelEither : Result a b -> b -> b
peelEither m d =
    case m of
        Ok s -> s
        _ -> d

catMaybes : List (Maybe a) -> List a
catMaybes l =
    List.map (\x -> case x of
        Just s -> [s]
        Nothing -> []) l |> List.concat
