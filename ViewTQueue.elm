module ViewTQueue exposing (viewTQueue)

import List exposing (..)
import Strings exposing (..)
import Prelude exposing (..)
import Helper exposing (..)
import Models exposing (..)
import Html.Attributes exposing (class, type_, href, class, style, classList, value, max, id, placeholder, attribute, value, src, disabled, attribute)
import Html.Events exposing (onClick, on, onInput)
import Html exposing (Html, button, div, text, p, a, section, h1, h2, table, thead, tr, th, tfoot, td, tbody, i, span, progress, form, input, ul, li, label, input, figure, img, br)
import ExternalQueryResult exposing (ExternalQueryResult)
import Html.Events.Extra exposing (onEnter)

viewTQueue : Model -> Html Msg
viewTQueue model = 
    [ div [ class "columns" ]
      [ div [ class "column is-6 is-mobile" ] 
        [ viewForm model
        ]
      , div [ class "column is-6 is-mobile" ]
        [ viewQueryResults model
        ]
      ]
    ] |> divHolderWithAttr [ onEnter FormValidate ]


viewQueryResults : Model -> Html Msg
viewQueryResults model =
    let display = case model.remoteQueryStatus of
            RIdle -> [ viewIdle ]
            RQuerying -> [ viewQuerying ]
            RError -> [ viewError ]
            ROk -> (viewQueryResults_ model.externalQueryResultList) ++ [hGap 80]
    in
    divHolderWithAttr [ class "box" ] 
    <| display

viewError : Html Msg
viewError =
    div [ flexStyle ]
    [ faIcon [ ("is-big", True) ] "times" 
    , h2 []
      [ text "Query failed." ]
    ]

viewQuerying : Html Msg
viewQuerying =
    figure [ class "image is-square" ]
    [ img [ src "loading.gif" ] []
    ]

viewIdle : Html Msg
viewIdle =
    div [ flexStyle ]
    [ faIcon [ ("is-big", True) ] "check" 
    , hGap 20
    , h2 []
      [ text "Enter something to query about." ]
    ]


viewQueryResults_ : List (ExternalQueryResult) -> List (Html Msg)
viewQueryResults_ m =
    flip map m (\r ->
        [ [ div [ class "column is-6 is-6-mobile" ]
          [ h2 
            [ style 
              [ ("text-align", "left") 
              , ("text-weight", "900")
              ] 
            ]
            [ text r.songName
            ]
          ]
        , div [ class "column is-6 is-6-mobile" ]
          [ p 
            [ style 
              [ ("text-align", "right") 
              ] 
            ]
            [ text <| formatSeconds r.songLength
            ]
          ] 
        ]
        |> wrapInDivColumn
        ,
        div [ class "column is-4 is-4-mobile" ]
          [ p
            [ style 
              [ ("text-align", "left") 
              ] 
            ]
            (
            [ text <| "-- " ++ r.songSinger
            , br [] []
            ]
            ++
            case r.songAlbum of
                Just s -> [ text s ]
                Nothing -> []
            )
          ]
        , div [ class "column is-8 is-8-mobile" ]
          [
          ]
        |> wrapInDivColumn_
        ,
        viewSubmitToServerButtons r
        ]
        ) |> concat

viewSubmitToServerButtons : ExternalQueryResult -> Html Msg
viewSubmitToServerButtons r =
    ( [ div [ class "is-grouped field", flexStyle ]
    [ p [ class "control" ]
        [ a [ classList [ ("button", True) 
                        , ("is-primary", True)
                        , ("is-loading", r.status == ExternalQueryResult.EDownloading)
                        ]
            , flip attribute "" (if r.status == ExternalQueryResult.EDownloading then "disabled" else "data")
            , onClick << NewSongToServer <| (r.songName, "m4a", r.songURL, r.songSinger ) 
            ]
            [ case r.status of
                ExternalQueryResult.EIdle -> text "Queue"
                ExternalQueryResult.EDownloading -> text ""
                ExternalQueryResult.EFailed -> text "Retry"
                ExternalQueryResult.ESuccess -> faIcon [] "check"
            ] 
        ]
    , p [ class "control" ]
        [ a [ class "button is-primary", attribute "disabled" "" ] 
            [ text "Queue Next" ]
        ]
    , p [ class "control" ]
        [ a [ class "button is-primary", href r.songURL ]
            [ text "Download" ]
        ]
    ]
    , hGap 10 ] ) |> divHolderWithAttr [ style [ ("border-bottom", "1px solid #ccc") ] ]


viewForm : Model -> Html Msg
viewForm model =
    let err = model.formState == Invalid
    in
    [ div [ class "field" ]
        [ label [ class "label" ] [ text "Query" ]
        , p [ class "control has-icons-left has-icons-right" ]
            [ input 
                [ classList 
                    [ ("input", True)
                    , ("is-danger", err)
                    ]
                , type_ "text"
                , placeholder "search" 
                , value model.formString
                , onInput SongNameInput
                ] 
                []
            , smallLeftIcon
            , if err then smallRightIcon else emptySpan
            ]
        , if err then helpText else emptySpan
        ]
    , apiButtons model
    , submitButtons
    ] |> divHolder

smallRightIcon : Html Msg
smallRightIcon =
    span [ class "icon is-small is-right" ]
    [  i [ class "fa fa-times" ] []
    ]

helpText : Html Msg
helpText =
    p [ class "help is-danger" ]
    [ text "Empty Query" ]

apiButtons : Model -> Html Msg
apiButtons model =
    let mapfn (api, apistr, h) = 
        p [ class "control"] 
            [ a [ classList 
                    [ ( "button", True )
                    , ( "is-primary is-focus", model.currApi == api )
                    ]
                , onClick <| ChangeAPI api ]
                [ h
                , span [] [ text apistr ]
                ]
            ]
    in
        div [ class "field has-addons", flexStyle ]
            <| map mapfn <| externalMusicAPIWithIcon faIconSmall

submitButtons : Html Msg
submitButtons =
    div [ class "is-grouped field", flexStyle ]
    [ p [ class "control" ]
        [ a [ class "button is-primary", onClick FormValidate ]
            [ text "Search" ] 
        ]
    , p [ class "control" ]
        [ a [ class "button is-link", onClick FormClear ] 
            [ text "Clear" ]
        ]
    ]

