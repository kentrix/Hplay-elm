module Socket exposing (..)

import Json.Decode exposing (..)
import Models exposing (..)
import WebSocket
import Time
import Ports

{- Subscriptions -}
subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch 
    [ WebSocket.listen model.wlHref Models.FromServer
    , Time.every Time.second Models.TimeTick
    , Ports.qqProxyReply RemoteQQAPIReply 
    ]
