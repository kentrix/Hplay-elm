module ExternalQueryResult exposing(..)

import List

type alias ExternalQueryResult =
    { songID : String
    , songURL : String
    , songImgUrl : Maybe String
    , songLyricsUrl : Maybe String
    , songName : String
    , songAlbum : Maybe String
    , songSinger : String
    , songLength : Int
    , apiSpecInfo : Maybe APISpecInfo
    , status : ExternalQueryResultStatus
    }

type APISpecInfo = ASIQQ

type ExternalQueryResultStatus = EDownloading | EFailed | ESuccess | EIdle

mapOverExternalQueryResultStatus : Maybe String 
   -> ExternalQueryResultStatus 
   -> List ExternalQueryResult 
   -> List ExternalQueryResult
mapOverExternalQueryResultStatus s st l =
    case s of
        Just s_ -> 
            flip List.map l (\x ->
                if x.songURL == s_
                then { x | status = st }
                else x)
        _ -> l
