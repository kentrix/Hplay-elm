module Config exposing (..)

librarySongPerPage : Float
librarySongPerPage = 10.0

libraryPagination : Int
libraryPagination = 2
