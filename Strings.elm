module Strings exposing (..)

sBrandName : String 
sBrandName = "HPlay"

sBrandSubtitle : String
sBrandSubtitle = "A music playing thing"

sTableHeads : List String
sTableHeads = ["Num", "Song Name", "Length", "Controls"]

sTabs : List String
sTabs = ["Playlist", "Library", "New Queue"]
