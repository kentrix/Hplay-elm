module View exposing (..)

import Html.Events exposing (onClick, on, onInput)
import List exposing (..)
import Strings exposing (..)
import Debug exposing (log)
import Prelude exposing (..)
import Helper exposing (..)
import Models exposing (..)
import Helper exposing (..)
import Html.Attributes exposing (class, type_, href, class, style, classList, value, max, id)
import Html.Events exposing (onClick, on, onInput)
import Html exposing (Html, button, div, text, p, a, section, h1, h2, table, thead, tr, th, tfoot, td, tbody, i, span, progress, form, input, ul, li, br)

import ViewCommon exposing (..)
import ViewTQueue exposing (viewTQueue)
import ViewTLibrary exposing (viewTLibrary)

viewTPlaylist : Model -> Html Msg
viewTPlaylist model = 
    divHolder $
    [ div [ class "columns is-desktop" ]
      [ div [ class "column is-three-quarters" ] 
        [ musicTable model
        ]
      , div [ class "column is-3" ]
        [ notiContainer model
        , br [] []
        , miscButtons model
        ]
      ]
    ]

miscButtons : Model -> Html Msg
miscButtons model =
    let items = safeGet model.currPos model.playList 
        isEmpty = List.isEmpty items
    in
    ( [ div [ class "is-grouped field", flexStyle ]
    [ p [ class "control" ]
        [ a [ classList [ ("button", True) 
                        , ("is-primary", True)
                        ]
            , onClick PurgePlaylist
            ]
            [ text "Purge Playlist"
            ] 
        ]
    , p [ class "control" ]
        [ a [ class "button is-primary", href "http://localhost:8081"]
            [ text "Don't Click Me!" ]
        ]
    ]
    , hGap 50 ] ) |> divHolder


viewController : Model -> Html Msg
viewController model =
    div [ class "columns" ]
      [ div [ class "column is-mobile" ]
        [ controller model ]
      ]

controller : Model -> Html Msg
controller model = 
    let (depFa, depCtrl) = 
            case model.playerStatus of
                Playing -> ("pause", Pause)
                Paused -> ("play", Play)
                Stopped -> ("play", Play)
        controlItem = map (String.append "fa fa-") ["fast-backward", depFa, "fast-forward"] 
        controlMsg = [ SeekPrev, depCtrl, SeekNext ]
        btnAttr = class "button is-primary"
        ext it = case it of
            Nothing -> 0
            Just {len} -> len
        songLength = ext << get model.currPos <| model.playList
    in
    div [ controllerStyle, class "box" ] 
    [ div [ class "columns" ]
      ( [ div [ class "column is-2 is-mobile" ]
          [ div [ class "field has-addons"] 
            ( map2 (\item ctrl ->
                p [ class "control" ]
                [ a [ class "button is-primary", onClick ctrl ]
                  [ span [ class "icon is-small"] 
                    [ i [ class item ] []
                    ] 
                  ]
                ]
                   ) controlItem controlMsg
            )
          ]
        ]
      ++
      [ div [ class "column is-7 is-mobile", flexStyle ]
        [ input
          [ type_ "range"
          , Html.Attributes.min "0" 
          , Html.Attributes.max << toString <| songLength
          , value $ toString model.currTime
          , onInput SeekControl
          , style [ ("width", "85%") ] ] 
          []
          , text $ formatSeconds2 model.currTime songLength
        ]
      ] 
      ++
      [ div [ class "column is-3 is-mobile", flexStyle ]
        [ div [ class "field has-addons" ]
          [ p [ class "control" ] 
            [ a [ class "button is-white" ]
              [ span [ class "icon is-small" ] 
                [ i [ class "fa fa-volume-up" ] 
                  []
                ]
              ]
            ]
          , p [ class "control", flexStyle] 
            [ input 
              [ type_ "range"
              , Html.Attributes.min "0" 
              , Html.Attributes.max "100"
              , value $ toString model.volume
              , onInput VolumeControl
              , style [ ("width", "auto") ] 
              ] 
              []
            ]
          ]
        ]
      ]
      )
    ] 

notificationToString : NotifcationType -> String -> String
notificationToString s i = 
    case s of  {- only this kind of pattern matching -}
        NQueued -> "queued"
        NBumped -> "bumped"
        NStopped -> "stopped"
        NPaused -> "paused"
        NDeleted -> "deleted"
        NPurged -> "purged"

notiContainer : Model -> Html Msg
notiContainer model = 
    divHolderWithAttr [ class "box" ]  $ map (\{pos, notificationType, detail, actor} -> 
          div [ class "notification" ] 
          [ button [ class "delete", onClick $ DelNotification pos ] []
          , a [] [ text actor ] 
          , text $ String.append " " (notificationToString notificationType detail)
          ]
          ) model.notificationList

viewHeader : Html Msg
viewHeader = 
    section [ class "hero is-primary" ]
    [ div [ class "hero-body" ]
      [ div [ class "container"] 
        [ h1 [ class "title"] 
          [ text sBrandName ]
        , h2 [ class "subtitle"] 
          [ text sBrandSubtitle ]
        ] 
      ]
    ]

musicTable : Model -> Html Msg
musicTable model =
    let ths = map (\t -> th [] [ text t ]) $ sTableHeads
        trs = map (\{pos, name, len, origin} -> tr [ class <| if model.currPos == pos then "is-selected" else ""]
                    [ td [] [ text $ toString (pos+1) ]
                    , td [] [ text name ]
                    , td [] [ text $ formatSeconds len ]
                    , td [] $ [ controls model pos ]
                    ]) model.playList 
    in
    table [ class "table" ]
    [ thead [] 
      [ tr [] 
        ths  
      ]
    , tfoot []
      [ tr [] 
        ths 
      ]
    , tbody []
        trs 
    ]

controls : Model -> Pos -> Html Msg
controls model pos = 
    let controlItem = map (String.append "fa fa-") ["play", "times", "arrow-up"] 
        btnAttr =  classList <| (map2 (,) ["button", "is-small", "is-primary"] (repeat 3 True)) ++ [("is-inverted", model.currPos == pos)]
        controlAction = [ PlaySong, DelSong, BumpSong ]
    in 
    map2 (\item a_ ->
        [ a [btnAttr, onClick $ a_ pos, controlStyle ]
          [ span [ class "icon is-small" ]
            [ i [ class item ] [] 
            ]
          ]
        ])
        controlItem controlAction
    |> concat |> divHolder

viewContentByTab : Model -> Html Msg
viewContentByTab model =
    case model.currTab of
        TPlaylist -> viewTPlaylist model
        TLibrary -> viewTLibrary model
        TQueue -> viewTQueue model

tabDisplay : Model -> Html Msg
tabDisplay model =
    div [ class "tabs" ]
    [ ul [] <|
        map2 (\x y ->
            li [ class $ if model.currTab == y then "is-active" else "" ] 
            [ a [ onClick <| ChangeTab y ] 
                [ text x ] 
            ]) sTabs allTabs
    ]


