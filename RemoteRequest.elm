module RemoteRequest exposing(qqAPIRequest)
import Http
import QQAPIJson exposing(decodeQQAPIReplyObj)
import Models exposing (..)
import List exposing (map)
import Ports exposing (..)

qqAPIRequest : String -> Cmd Msg
qqAPIRequest s =
    let songsToGet = 5
        url = "http://s.music.qq.com/fcgi-bin/music_search_new_platform?t=0&n=" ++ toString songsToGet ++ "&aggr=1&cr=1&loginUin=0&format=json&inCharset=GB2312&outCharset=utf-8&notice=0&platform=jqminiframe.json&needNewCode=0&p=1&catZhida=0&remoteplace=sizer.newclient.next_song&w=" ++ s
        headers =
            [ ("Origin", "http://qq.com")
            , ("Access-Control-Request-Method", "POST")
            , ("Access-Control-Request-Headers", "X-Custom-Header")
            ] |> map (uncurry Http.header)
        request = Http.request
            { method = "GET"
            , headers = headers
            , url = url
            , body = Http.emptyBody
            , expect = Http.expectJson decodeQQAPIReplyObj
            , timeout = Just 10000.0
            , withCredentials = False
            }
    in
        qqProxyPost url
