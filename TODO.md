### Improved with some sort of priority
* ~~Implement seek~~ Done
* Add current playing song in the bottom bar
* Improve display in the main play list
* Library display and search 80%
    * Add a no result screen
* Use server to handle song queries instead of proxies
* More interactive queuing screen
* Implement other music APIs (On server?)
* Lyrics feature
* Mobile design
* Better query page looks
* Volume control (Looks like MPD can't provide at times)
* Clean up code (Both server and client)

