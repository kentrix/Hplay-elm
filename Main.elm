import Html exposing (Html, button, div, text, p, a, section, h1, h2, table, thead, tr, th, tfoot, td, tbody, i, span, progress, form, input)
import Html.Attributes exposing (class, type_)
import Html.Events exposing (onClick, on, onInput)
import List exposing (..)
import Strings exposing (..)
import Html.Attributes exposing (href, class, style, classList, value, max, id)
import Debug exposing (log)
import Prelude exposing (..)
import Array
import Task
import Helper exposing (..)

import Models exposing (..)
import View exposing (..)
import Socket exposing (subscriptions)
import WebSocket
import ClientJson exposing (..)
import Json.Decode exposing (decodeString)
import Time exposing (Time, second)
import RemoteRequest exposing (qqAPIRequest)
import QQAPIJson exposing (decodeQQAPIReplyObj, decodeQQReplyToResult)
import Tasks exposing (libraryMatchTask)
import ExternalQueryResult exposing(..)

main : Program Flags Model Msg
main =
    Html.programWithFlags
    { init = initModel
    , view = view
    , subscriptions = subscriptions
    , update = update
    }

{- Update -}
update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        DelNotification n -> 
            noCmd { model | notificationList = take n model.notificationList ++ reducePos $ drop (n+1) model.notificationList }
        {- Player Control -}
        SeekNext ->
            (model, WebSocket.send model.wlHref "{\"rType\":\"RSeekNext\"}")
        SeekPrev ->
            (model, WebSocket.send model.wlHref "{\"rType\":\"RSeekPrev\"}")
        SeekControl n -> 
            case String.toInt n of
               Ok v -> ( model , WebSocket.send model.wlHref <|
                   ("{\"rType\":\"RSeek\",\"rInt\":" ++ n ++ "}"))
               _ -> (log (toString msg) model, Cmd.none)
        VolumeControl n ->
            case String.toInt n of
               Ok v -> ( {model | volume = Just v }, Cmd.none )
               _ -> (log (toString msg) model, Cmd.none)
        Play ->
            (model, WebSocket.send model.wlHref "{\"rType\":\"RPlay\"}")
        Pause ->
            (model, WebSocket.send model.wlHref "{\"rType\":\"RPause\"}")
        PlaySong n ->
            (model, WebSocket.send model.wlHref <| 
                ("{\"rType\":\"RPlay\",\"rInt\":"++ toString n ++ "}")
            )
        DelSong n ->
            (model, WebSocket.send model.wlHref <| 
                ("{\"rType\":\"RDel\",\"rInt\":"++ toString n ++ "}")
            )
        PurgePlaylist ->
            (model, WebSocket.send model.wlHref <| 
                ("{\"rType\":\"RPurge\"}")
            )
        {- From Server -}
        FromServer n ->
            noCmd <| decodeAndUpdateModel model n
        {- *From Server* -}
        TimeTick t ->
            case model.playerStatus of
                Playing -> noCmd { model | currTime = model.currTime + 1}
                _ -> (model, Cmd.none)
        ChangeTab t ->
            if t == model.currTab
                then (model, Cmd.none)
                else noCmd { model | currTab = t }
        {- Queue API -}
        SongNameInput s ->
            noCmd { model | formString = s }
        FormValidate ->
            if String.isEmpty model.formString
                then noCmd { model | formState = Invalid }
                else ( { model | formState = Valid, remoteQueryStatus = RQuerying }
                     , mkAndSendQuery model
                     )
        FormClear ->
            noCmd { model | formString = "" }
        ChangeAPI api ->
            noCmd { model | currApi = api }
        RemoteQQAPIReply r ->
            noCmd <| decodeAndUpdateResultQQ model r
        NewSongToServer (name, ext, url, singerName) ->
            (model, WebSocket.send model.wlHref
            ( "{" ++
            "\"rType\":\"RNew\"" ++
            ",\"rName\":\"" ++ name ++ "\"" ++
            ",\"rExt\":\"" ++ ext ++ "\"" ++
            ",\"rURL\":\"" ++ url ++ "\"" ++
            ",\"rSingerName\":\"" ++ singerName ++ "\"" ++
            "}" )
            )
        {- Library -}
        LibraryPageChange n ->
            noCmd { model | librarySearchCurrPage = n }
        DoLibrarySearch ->
            (model, Task.perform LibraryUpdateFilter
             <| libraryMatchTask model.librarySearchString model )
        LibrarySearchFormClear ->
            noCmd { model | librarySearchResult = Nothing, librarySearchString = "" }
        LibraryUpdateFilter ml ->
            noCmd { model | librarySearchResult = ml }
        LibrarySearchInput s ->
            noCmd { model | librarySearchString = s }
        {- Library player control -}
        QueueSongAtEnd s ->
            (model, WebSocket.send model.wlHref
            ( "{" ++
            "\"rType\":\"RAddEnd\"" ++
            ",\"rName\":\"" ++ s ++ "\"" ++
            "}" )
            )
        QueueSongAtNext s ->
            (model, WebSocket.send model.wlHref
            ( "{" ++
            "\"rType\":\"RAddNext\"" ++
            ",\"rName\":\"" ++ s ++ "\"" ++
            "}" )
            )
        QueueSongAtNextAndPlay s ->
            (model, WebSocket.send model.wlHref
            ( "{" ++
            "\"rType\":\"RAddAndPlayNow\"" ++
            ",\"rName\":\"" ++ s ++ "\"" ++
            "}" )
            )
        _ ->
            (log (toString msg) model, Cmd.none)

decodeAndUpdateResultQQ : Model -> String -> Model
decodeAndUpdateResultQQ model s =
    let res = decodeString decodeQQAPIReplyObj s
        c = log "" res
    in
        case res of
            Ok r -> { model | externalQueryResultList = decodeQQReplyToResult r 
                            , remoteQueryStatus = ROk }
            Err e -> { model | remoteQueryStatus = RError }

mkAndSendQuery : Model -> Cmd Msg
mkAndSendQuery model =
    case model.currApi of
        QQ -> qqAPIRequest <| log ("to QQ Json") model.formString
        _ -> Cmd.none

decodeAndUpdateModel : Model -> String -> Model
decodeAndUpdateModel model s =
    let decoded = decodeString decodeServerData s
        _ = log (toString decoded) model
    in case decoded of
        Ok r -> if r.iType == "IUpdate"
                    then
                    { model |
                      playList = peelMaybe r.iSongList model.playList
                    , currPos = peelMaybe r.iCurrPos model.currPos
                    , volume = r.iVolume
                    , playerStatus = case r.iPlayerStatus of
                        Just x -> case x of
                            "Playing" -> Playing
                            "Stopped" -> Stopped
                            "Paused" -> Paused
                            _ -> Stopped
                        _ -> Stopped
                    , library = peelMaybe r.iLibrary model.library
                    , currTime = peelMaybe r.iCurrTime model.currTime
                    }
                else if r.iType == "IQueueAck"
                then
                    { model |
                      externalQueryResultList = 
                          mapOverExternalQueryResultStatus r.iDetail EDownloading model.externalQueryResultList
                    }
                else if r.iType == "IQueueSuccess"
                then
                    { model |
                      externalQueryResultList = 
                          mapOverExternalQueryResultStatus r.iDetail ESuccess model.externalQueryResultList
                    }
                else if r.iType == "IQueueFailed"
                then
                    { model |
                      externalQueryResultList = 
                          mapOverExternalQueryResultStatus r.iDetail EFailed model.externalQueryResultList
                    }
                else model
        Err e -> model

{- Views -}
view : Model -> Html Msg
view model =
    divHolder 
        [ viewHeader 
        , hGap 50
        , flip containerise [] $ tabDisplay model
        , hGap 20
        , flip containerise [] $ viewContentByTab model
        , viewController model ]
