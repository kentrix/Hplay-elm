module Helper exposing (..)
import List exposing (..)
import Strings exposing (..)
import Models exposing (..)

import Html.Attributes exposing (class, type_, href, class, style, classList, value, max, id)
import Html.Events exposing (onClick, on, onInput, keyCode)
import Html exposing (Html, Attribute, div, span, i)
import Json.Decode

formatSeconds : Int -> String
formatSeconds n = 
    let min = n // 60
        sec = rem n  60
        minStr = toString min
        secStr = toString sec
    in
        minStr ++ ":" ++ (if sec < 10 then "0" else "") ++ toString sec 

formatSeconds2 : Int -> Int -> String
formatSeconds2 n m =
    formatSeconds n ++ "/" ++ formatSeconds m

reducePos : List NotificationItem -> List NotificationItem
reducePos = map (\i ->
    { i | pos = (\{pos} -> pos - 1) i } )

divHolder : List (Html Msg) -> Html Msg
divHolder html = 
    div [] html

divHolderWithAttr : List (Html.Attribute a) -> List (Html a) -> Html a
divHolderWithAttr attr html =
    div attr html

hGap : Int -> Html Msg
hGap size = 
   div [ style [ ( "height", toString size ++ "px" ) ] ] []

containerise : Html a -> List (Html.Attribute a) -> Html a
containerise html attr =
    div ( [ class "container" ] ++ attr)  [ html ]

faIcon : List (String, Bool) -> String -> Html Msg
faIcon cl fi =
    span [ classList <| [ ("icon", True) ] ++ cl ]
    [ i [ class <| "fa fa-" ++ fi ] 
        []
    ]

faIconSmall : String -> Html Msg
faIconSmall =
    faIcon  [ ("is-small", True) ]

noCmd : Model -> (Model, Cmd a)
noCmd m = ( m, Cmd.none )

emptySpan : Html a
emptySpan = span [] []

externalMusicAPIList : List ExternalMusicAPI
externalMusicAPIList = [ QQ , Netease , Youtube , Baidu , Bilibili , Soundcloud ]

externalMusicAPIs : List (ExternalMusicAPI, String)
externalMusicAPIs =
    map2 (,) externalMusicAPIList 
    ["QQ", "163", "Youtube", "Baidu", "Bilibili", "Soundcloud"]

externalAPIFaStringList : List String
externalAPIFaStringList = [ "qq", "netease", "youtube", "baidu", "bilibili", "soundcloud"]

externalMusicAPIWithIcon : (String -> Html Msg) -> List (ExternalMusicAPI, String, Html Msg)
externalMusicAPIWithIcon f =
    let a = map f externalAPIFaStringList
    in
        map2 (\(x, y) z -> (x, y, z)) externalMusicAPIs a
    
allTabs : List Tabs
allTabs = [TPlaylist, TLibrary, TQueue]

flexStyle : Html.Attribute a
flexStyle =
    style [ ("display", "flex")
          , ("align-items", "center")
          , ("justify-content", "center") 
          ]

wrapInDivColumnWithAttr : List (Html a) -> Html.Attribute a -> Html a
wrapInDivColumnWithAttr l attr =
    div [ class "columns", attr] l

wrapInDivColumnWithAttr_ : Html a -> Html.Attribute a -> Html a
wrapInDivColumnWithAttr_ i attr =
    flip wrapInDivColumnWithAttr attr [i]

wrapInDivColumn : List (Html a) -> Html a
wrapInDivColumn =
    div [ class "columns" ]

wrapInDivColumn_ : Html a -> Html a
wrapInDivColumn_ a =
    wrapInDivColumn [ a ]

loadingIcon : Html a
loadingIcon =
    div [ class "loader" ]
    <| map (\n ->
      div [ class "square", id <| "sqr" ++ toString n ] [] ) (range 0 6)

toWebSocketAddr : String -> String
toWebSocketAddr s =
    if String.startsWith "http://" s then
        "ws://" ++ String.dropLeft 6 s
    else if String.startsWith "https://" s then
        "wss://" ++ String.dropLeft 7 s
    else s

smallLeftIcon : Html Msg
smallLeftIcon =
    span [ class "icon is-small is-left" ]
    [  i [ class "fa fa-search" ] []
    ]

displayAttr : Bool -> Html.Attribute a
displayAttr b =
    if b then style [] else style [ ("display", "none") ]

fstnsnd4 : (a, b, c, d) -> (a, b)
fstnsnd4 a =
    case a of
        (a_, b_, _, _) -> (a_, b_)

extractPosAndName : SongItem -> (Int, String)
extractPosAndName i =
    (i.pos, i.name)

