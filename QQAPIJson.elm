module QQAPIJson exposing (..)
import Json.Encode
import Json.Decode
import Json.Decode.Pipeline
import ExternalQueryResult exposing (ExternalQueryResult, ExternalQueryResultStatus)
import Prelude exposing (..)
import List
import String

type alias QQAPIReplySongListItem =
    {  {- chinesesinger : Int
    , docid : String
    -}
    f : String
    {-
    , fiurl : String
    , fnote : Int
    , fsinger : String
    , fsinger2 : String
    -}
    , fsong : String
    , grp : List QQAPIReplyGrpItem
    {-
    , isupload : Int
    , isweiyun : Int
    , lyric : String
    , mv : String
    , nt : Int
    , only : Int
    , pubTime : Int
    , pure : Int
    , singerMID : String
    , singerMID2 : String
    , singerid : Int
    , singerid2 : Int
    , t : Int
    , tag : Int
    , ver : Int
    -}
    }

decodeQQAPIReplySongListItem : Json.Decode.Decoder QQAPIReplySongListItem
decodeQQAPIReplySongListItem =
    Json.Decode.Pipeline.decode QQAPIReplySongListItem
        {-
        |> Json.Decode.Pipeline.required "chinesesinger" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "docid" (Json.Decode.string)
        -}
        |> Json.Decode.Pipeline.required "f" (Json.Decode.string)
        {-
        |> Json.Decode.Pipeline.required "fiurl" (Json.Decode.string)
        |> Json.Decode.Pipeline.required "fnote" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "fsinger" (Json.Decode.string)
        |> Json.Decode.Pipeline.required "fsinger2" (Json.Decode.string)
        -}
        |> Json.Decode.Pipeline.required "fsong" (Json.Decode.string)
        |> Json.Decode.Pipeline.required "grp" (Json.Decode.list decodeQQAPIReplyGrpItem)
        {-
        |> Json.Decode.Pipeline.required "isupload" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "isweiyun" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "lyric" (Json.Decode.string)
        |> Json.Decode.Pipeline.required "mv" (Json.Decode.string)
        |> Json.Decode.Pipeline.required "nt" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "only" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "pubTime" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "pure" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "singerMID" (Json.Decode.string)
        |> Json.Decode.Pipeline.required "singerMID2" (Json.Decode.string)
        |> Json.Decode.Pipeline.required "singerid" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "singerid2" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "t" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "tag" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "ver" (Json.Decode.int)
        -}

encodeQQAPIReplySongListItem : QQAPIReplySongListItem -> Json.Encode.Value
encodeQQAPIReplySongListItem record =
    Json.Encode.object
        [ {- ("chinesesinger",  Json.Encode.int <| record.chinesesinger)
        , ("docid",  Json.Encode.string <| record.docid)
        -}
        ("f",  Json.Encode.string <| record.f)
        {-
        , ("fiurl",  Json.Encode.string <| record.fiurl)
        , ("fnote",  Json.Encode.int <| record.fnote)
        , ("fsinger",  Json.Encode.string <| record.fsinger)
        , ("fsinger2",  Json.Encode.string <| record.fsinger2)
        -}
        , ("fsong",  Json.Encode.string <| record.fsong)
        , ("grp",  Json.Encode.list <| List.map encodeQQAPIReplyGrpItem <| record.grp)
        {-
        , ("isupload",  Json.Encode.int <| record.isupload)
        , ("isweiyun",  Json.Encode.int <| record.isweiyun)
        , ("lyric",  Json.Encode.string <| record.lyric)
        , ("mv",  Json.Encode.string <| record.mv)
        , ("nt",  Json.Encode.int <| record.nt)
        , ("only",  Json.Encode.int <| record.only)
        , ("pubTime",  Json.Encode.int <| record.pubTime)
        , ("pure",  Json.Encode.int <| record.pure)
        , ("singerMID",  Json.Encode.string <| record.singerMID)
        , ("singerMID2",  Json.Encode.string <| record.singerMID2)
        , ("singerid",  Json.Encode.int <| record.singerid)
        , ("singerid2",  Json.Encode.int <| record.singerid2)
        , ("t",  Json.Encode.int <| record.t)
        , ("tag",  Json.Encode.int <| record.tag)
        , ("ver",  Json.Encode.int <| record.ver)
        -}
        ]

type alias QQAPIReplyGrpItem =
    { 
    {- 
    chinesesinger : Int
    , docid : String
    -}
    f : String
    {-
    , fiurl : String
    , fnote : Int
    , fsinger : String
    , fsinger2 : String
    , fsong : String
    , isupload : Int
    , isweiyun : Int
    , lyric : String
    , mv : String
    , nt : Int
    , only : Int
    , pubTime : Int
    , pure : Int
    , singerMID : String
    , singerMID2 : String
    , singerid : Int
    , singerid2 : Int
    , t : Int
    , tag : Int
    , ver : Int
    -}
    }

decodeQQAPIReplyGrpItem : Json.Decode.Decoder QQAPIReplyGrpItem
decodeQQAPIReplyGrpItem =
    Json.Decode.Pipeline.decode QQAPIReplyGrpItem
        {-
        |> Json.Decode.Pipeline.required "chinesesinger" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "docid" (Json.Decode.string)
        -}
        |> Json.Decode.Pipeline.required "f" (Json.Decode.string)
        {-
        |> Json.Decode.Pipeline.required "fiurl" (Json.Decode.string)
        |> Json.Decode.Pipeline.required "fnote" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "fsinger" (Json.Decode.string)
        |> Json.Decode.Pipeline.required "fsinger2" (Json.Decode.string)
        |> Json.Decode.Pipeline.required "fsong" (Json.Decode.string)
        |> Json.Decode.Pipeline.required "isupload" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "isweiyun" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "lyric" (Json.Decode.string)
        |> Json.Decode.Pipeline.required "mv" (Json.Decode.string)
        |> Json.Decode.Pipeline.required "nt" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "only" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "pubTime" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "pure" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "singerMID" (Json.Decode.string)
        |> Json.Decode.Pipeline.required "singerMID2" (Json.Decode.string)
        |> Json.Decode.Pipeline.required "singerid" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "singerid2" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "t" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "tag" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "ver" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "fiurl" (Json.Decode.string)
        |> Json.Decode.Pipeline.required "fnote" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "fsinger" (Json.Decode.string)
        |> Json.Decode.Pipeline.required "fsinger2" (Json.Decode.string)
        |> Json.Decode.Pipeline.required "fsong" (Json.Decode.string)
        |> Json.Decode.Pipeline.required "isupload" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "isweiyun" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "lyric" (Json.Decode.string)
        |> Json.Decode.Pipeline.required "mv" (Json.Decode.string)
        |> Json.Decode.Pipeline.required "nt" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "only" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "pubTime" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "pure" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "singerMID" (Json.Decode.string)
        |> Json.Decode.Pipeline.required "singerMID2" (Json.Decode.string)
        |> Json.Decode.Pipeline.required "singerid" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "singerid2" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "t" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "tag" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "ver" (Json.Decode.int)
        -}

encodeQQAPIReplyGrpItem : QQAPIReplyGrpItem -> Json.Encode.Value
encodeQQAPIReplyGrpItem record =
    Json.Encode.object
        [ {- ("chinesesinger",  Json.Encode.int <| record.chinesesinger)
        , ("docid",  Json.Encode.string <| record.docid)
        -}
         ("f",  Json.Encode.string <| record.f)
        {-
        , ("fiurl",  Json.Encode.string <| record.fiurl)
        , ("fnote",  Json.Encode.int <| record.fnote)
        , ("fsinger",  Json.Encode.string <| record.fsinger)
        , ("fsinger2",  Json.Encode.string <| record.fsinger2)
        , ("fsong",  Json.Encode.string <| record.fsong)
        , ("isupload",  Json.Encode.int <| record.isupload)
        , ("isweiyun",  Json.Encode.int <| record.isweiyun)
        , ("lyric",  Json.Encode.string <| record.lyric)
        , ("mv",  Json.Encode.string <| record.mv)
        , ("nt",  Json.Encode.int <| record.nt)
        , ("only",  Json.Encode.int <| record.only)
        , ("pubTime",  Json.Encode.int <| record.pubTime)
        , ("pure",  Json.Encode.int <| record.pure)
        , ("singerMID",  Json.Encode.string <| record.singerMID)
        , ("singerMID2",  Json.Encode.string <| record.singerMID2)
        , ("singerid",  Json.Encode.int <| record.singerid)
        , ("singerid2",  Json.Encode.int <| record.singerid2)
        , ("t",  Json.Encode.int <| record.t)
        , ("tag",  Json.Encode.int <| record.tag)
        , ("ver",  Js on.Encode.int <| record.ver)
        -}
        ]

type alias QQAPISongObj =
    { curnum : Int
    , curpage : Int
    , list : List QQAPIReplySongListItem
    , totalnum : Int
    }

decodeQQAPISongObj : Json.Decode.Decoder QQAPISongObj
decodeQQAPISongObj =
    Json.Decode.Pipeline.decode QQAPISongObj
        |> Json.Decode.Pipeline.required "curnum" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "curpage" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "list" (Json.Decode.list decodeQQAPIReplySongListItem)
        |> Json.Decode.Pipeline.required "totalnum" (Json.Decode.int)

encodeQQAPISongObj : QQAPISongObj -> Json.Encode.Value
encodeQQAPISongObj record =
    Json.Encode.object
        [ ("curnum",  Json.Encode.int <| record.curnum)
        , ("curpage",  Json.Encode.int <| record.curpage)
        , ("list",  Json.Encode.list <| List.map encodeQQAPIReplySongListItem <| record.list)
        , ("totalnum",  Json.Encode.int <| record.totalnum)
        ]

type alias QQAPIDataObj =
    { song : QQAPISongObj
    }

decodeQQAPIDataObj : Json.Decode.Decoder QQAPIDataObj
decodeQQAPIDataObj =
    Json.Decode.Pipeline.decode QQAPIDataObj
        |> Json.Decode.Pipeline.required "song" (decodeQQAPISongObj)

encodeQQAPIDataObj : QQAPIDataObj -> Json.Encode.Value
encodeQQAPIDataObj record =
    Json.Encode.object
        [ ("song",  encodeQQAPISongObj <| record.song)
        ]

type alias QQAPIReplyObj =
    { code : Int
    , data : QQAPIDataObj
    , message : String
    , notice : String
    , subcode : Int
    , time : Int
    , tips : String
    }


decodeQQAPIReplyObj : Json.Decode.Decoder QQAPIReplyObj
decodeQQAPIReplyObj =
    Json.Decode.Pipeline.decode QQAPIReplyObj
        |> Json.Decode.Pipeline.required "code" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "data" (decodeQQAPIDataObj)
        |> Json.Decode.Pipeline.required "message" (Json.Decode.string)
        |> Json.Decode.Pipeline.required "notice" (Json.Decode.string)
        |> Json.Decode.Pipeline.required "subcode" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "time" (Json.Decode.int)
        |> Json.Decode.Pipeline.required "tips" (Json.Decode.string)


encodeQQAPIReplyObj : QQAPIReplyObj -> Json.Encode.Value
encodeQQAPIReplyObj record =
    Json.Encode.object
        [ ("code",  Json.Encode.int <| record.code)
        , ("data",  encodeQQAPIDataObj <| record.data)
        , ("message",  Json.Encode.string <| record.message)
        , ("notice",  Json.Encode.string <| record.notice)
        , ("subcode",  Json.Encode.int <| record.subcode)
        , ("time",  Json.Encode.int <| record.time)
        , ("tips",  Json.Encode.string <| record.tips)
        ]

decodeQQReplyToResult : QQAPIReplyObj -> List ExternalQueryResult
decodeQQReplyToResult obj =
    List.map qqsongDecoder obj.data.song.list

qqsongDecoder : QQAPIReplySongListItem -> ExternalQueryResult
qqsongDecoder si =
    let fstrsA = String.split "|" si.f
        fstrsB = String.split "@@" si.f
    in
        if List.length fstrsA > 3 then
        { songID = peelMaybe (List.head fstrsA)  "000"
        , songURL = ""
        , songImgUrl = Nothing
        , songLyricsUrl = Nothing
        , songName = si.fsong
        , songAlbum =  get 5 fstrsA
        , songSinger = peelMaybe (get 3 fstrsA) "No Singer Name!"
        , songLength = peelEither (String.toInt <| peelMaybe (get 7 fstrsA) "0" ) 0
        , apiSpecInfo = Nothing
        , status = ExternalQueryResult.EIdle
        } |> qqpopulate
        else
        { songID = "000"
        , songURL = peelMaybe (get 8 fstrsB) ""
        , songImgUrl = Nothing
        , songLyricsUrl = Nothing
        , songName = si.fsong
        , songAlbum =  get 2 fstrsB
        , songSinger = peelMaybe (get 3 fstrsB) "No Singer Name!"
        , songLength = peelEither (String.toInt <| peelMaybe (get 7 fstrsB) "0" ) 0
        , apiSpecInfo = Nothing
        , status = ExternalQueryResult.EIdle
        }


qqpopulate : ExternalQueryResult -> ExternalQueryResult
qqpopulate eqr =
    { eqr | songURL = qqsongURL eqr.songID
          , songImgUrl = Nothing {- TODO: Image -}
          , songLyricsUrl = Just <| qqsongLyricsUrl eqr.songID
    }

qqsongURL : String -> String
qqsongURL s =
    "http://ws.stream.qqmusic.qq.com/" ++ s ++ ".m4a?fromtag=46"

qqsongImgUrl : String -> String
qqsongImgUrl s = s

qqsongLyricsUrl : String -> String
qqsongLyricsUrl s =
    let n = String.toInt s
        r = case n of
                Ok res -> rem res 100
                _ -> 0
    in
        "http://music.qq.com/miniportal/static/lyric/" ++ toString r ++ "/" ++ s ++ ".xml"

