module Models exposing (..)

import Time exposing (Time)
import WebSocket
import Config exposing (..)
import QQAPIJson exposing (QQAPIReplyObj)
import Http
import ExternalQueryResult exposing (ExternalQueryResult)

type Msg = 
    UpdatePlayList
    | ChangeTab Tabs
    {- Notification -}
    | DelNotification Pos
    {- Playlist control -}
    | PlaySong Pos
    | DelSong Pos
    | BumpSong Pos
    | PurgePlaylist
    {- Player control -}
    | SeekNext
    | SeekPrev
    | Play
    | Pause
    | Stop
    | SeekControl String
    | VolumeControl String
    | TimeTick Time
    {- Library player control -}
    | QueueSongAtEnd String
    | QueueSongAtNext String
    | QueueSongAtNextAndPlay String
    {- Library -}
    | LibrarySearchInput String
    | DoLibrarySearch
    | LibrarySearchFormClear
    | LibraryPageChange Int
    | LibraryUpdateFilter (Maybe PlayList)
    {- Queue API -}
    | ChangeAPI ExternalMusicAPI
    | SongNameInput String 
    | FormValidate
    | FormClear
    {- Remote API Reply -}
    | RemoteQQAPIReply String
    {- Server API-}
    | FromServer String
    | NewSongToServer NewSongToServerInfo

type alias Flags = { wlHref : String }

type alias NewSongToServerInfo = ( String, String, String, String )

type ExternalMusicAPI = QQ | Netease | Youtube | Baidu | Bilibili | Soundcloud

type RemoteQueryStatus = RQuerying | RError | RIdle | ROk

type Tabs = TPlaylist | TLibrary | TQueue

type NotifcationType = 
    NQueued
    | NBumped
    | NStopped
    | NPaused
    | NDeleted
    | NPurged

type PlayerStatus =
    Playing
    | Paused
    | Stopped

type alias Pos = Int
type alias Length = Int
type alias CurrPos = Int
type alias SongName = String
type alias Origin = String
type alias Actor = String
type alias Detail = String

type alias SongItem = 
    { pos : Int
    , name : String
    , len : Int
    , origin : String
    }

type alias PlayList = List SongItem

type alias NotificationItem = 
    { pos : Int
    , notificationType : NotifcationType
    , detail : String
    , actor : String
    }
type alias NotificationList = List NotificationItem

type alias Model =
    { wlHref : String
    , playList : PlayList 
    , currPos  : CurrPos
    , notificationList : NotificationList 
    , currTime : Int
    , volume   : Maybe Int 
    , playerStatus : PlayerStatus
    , library  : PlayList
    , currTab  : Tabs
    , formString : String
    , formState: FormState
    , currApi  : ExternalMusicAPI
    , externalQueryResultList : List ExternalQueryResult
    , remoteQueryStatus : RemoteQueryStatus
    , librarySearchString : String
    , librarySearchStatus : LibrarySearchStatus
    , librarySearchCurrPage : Int
    {- It is faster(?) to store the result since we will be re-render the page
     - every second or so-}
    , librarySearchResult : Maybe PlayList
    }

type LibrarySearchStatus =
    LibrarySearching
    | LibrarySearchDone
    | LibrarySearchIdle

type FormState = Valid | Invalid

initModel : Flags -> (Model, Cmd msg)
initModel flag =
    ({ wlHref = flag.wlHref
    , playList = []
    , currPos = 0
    , notificationList = [
        { pos = 0
        , notificationType = NQueued
        , detail = "Test"
        , actor = "abc"
        }
        ,
        { pos = 1
        , notificationType = NQueued
        , detail = "Test2"
        , actor = "def"
        }
        ]
    , currTime = 50 
    , volume = Just 100
    , playerStatus = Stopped
    , library = []
    , currTab = TPlaylist
    , formString = ""
    , formState = Valid
    , currApi = QQ
    , externalQueryResultList = []
    , remoteQueryStatus = RIdle
    , librarySearchString = ""
    , librarySearchStatus = LibrarySearchIdle
    , librarySearchCurrPage = 1
    , librarySearchResult = Nothing
    }
    , WebSocket.send flag.wlHref "{\"rType\":\"RHello\"}")
