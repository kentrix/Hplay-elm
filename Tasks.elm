module Tasks exposing (..)

import Models exposing (..)
import Prelude exposing (..)
import Helper exposing (..)
import Task
import Regex
import List

import Debug exposing (log)


libraryMatchTask : String -> Model -> Task.Task Never (Maybe PlayList)
libraryMatchTask s model =
    let ss = String.words s
        sss = List.map (\x -> Regex.split Regex.All (Regex.regex ".*?") x ) ss
        regexList = List.map (Regex.caseInsensitive << Regex.regex) <| List.map (String.join ".*?") sss
        songNameList = List.map extractPosAndName model.library
    in
        List.foldl (\(index,name) a ->
            if List.any identity <| List.map (\r ->
               Regex.contains r name) regexList
              then index :: a
              else a ) [] songNameList
        |> flip safeGetList model.library
        |> Just
        |> Task.succeed
