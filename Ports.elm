port module Ports exposing (..)
import Models exposing (..)

port qqProxyReply : (String -> msg)  -> Sub msg

port qqProxyPost : String -> Cmd msg
