module ClientJson exposing (..)

import Json.Decode exposing (int, string, at, Decoder, nullable)
import Models exposing (..)
import Json.Encode
import Json.Decode
import Json.Decode.Pipeline

type InstructionType = 
    IUpdate
    | INotification
    | IPlay
    | IPause
    | IStop
    | IVolume
    | ISeek
    | IAck
    | IError

type RequestType =
    RUpdate
    | RNew
    | RPlay
    | RPause
    | RStop
    | RVolume
    | RSeek
    | RQueue
    | RHello
    | RDel
    | RAddNext
    | RAddEnd
    | RAddAndPlayNow
    | RSeekNext
    | RSeekPrev

type alias ServerData =
    { iType : String
    , iDetail : Maybe String
    , iSongList : Maybe (List SongItem)
    , iNotification : Maybe NotificationINotification 
    , iLibrary : Maybe (List SongItem)
    , iPlayerStatus : Maybe String
    , iVolume : Maybe Int
    , iCurrTime : Maybe Int
    , iCurrPos : Maybe Int
    }

type alias Notification =
    { iNotification : NotificationINotification
    }

type alias NotificationINotification =
    { notificationType : String
    , notificationDetail : String
    , actor : String
    }

decodeNotification : Json.Decode.Decoder Notification
decodeNotification =
    Json.Decode.Pipeline.decode Notification
        |> Json.Decode.Pipeline.required "iNotification" (decodeNotificationINotification)

decodeNotificationINotification : Json.Decode.Decoder NotificationINotification
decodeNotificationINotification =
    Json.Decode.Pipeline.decode NotificationINotification
        |> Json.Decode.Pipeline.required "notificationType" (Json.Decode.string)
        |> Json.Decode.Pipeline.required "notificationDetail" (Json.Decode.string)
        |> Json.Decode.Pipeline.required "actor" (Json.Decode.string)

encodeNotification : Notification -> Json.Encode.Value
encodeNotification record =
    Json.Encode.object
        [ ("iNotification",  encodeNotificationINotification <| record.iNotification)
        ]

encodeNotificationINotification : NotificationINotification -> Json.Encode.Value
encodeNotificationINotification record =
    Json.Encode.object
        [ ("notificationType",  Json.Encode.string <| record.notificationType)
        , ("notificationDetail",  Json.Encode.string <| record.notificationDetail)
        , ("actor",  Json.Encode.string <| record.actor)
        ]

decodeServerData : Json.Decode.Decoder ServerData
decodeServerData =
    Json.Decode.Pipeline.decode ServerData
        |> Json.Decode.Pipeline.required "iType" (Json.Decode.string)
        |> Json.Decode.Pipeline.required "iDetail" (nullable Json.Decode.string)
        |> Json.Decode.Pipeline.required "iSongList" (nullable <| Json.Decode.list decodeSongItem)
        |> Json.Decode.Pipeline.required "iNotification" (nullable <| decodeNotificationINotification )
        |> Json.Decode.Pipeline.required "iLibrary" (nullable <| Json.Decode.list decodeSongItem)
        |> Json.Decode.Pipeline.required "iPlayerStatus" (nullable Json.Decode.string)
        |> Json.Decode.Pipeline.required "iVolume" (nullable Json.Decode.int)
        |> Json.Decode.Pipeline.required "iCurrTime" (nullable Json.Decode.int)
        |> Json.Decode.Pipeline.required "iCurrPos" (nullable Json.Decode.int)

decodeSongItem : Json.Decode.Decoder SongItem
decodeSongItem =
    Json.Decode.Pipeline.decode SongItem
    |> Json.Decode.Pipeline.required "pos" (Json.Decode.int)
    |> Json.Decode.Pipeline.required "name" (Json.Decode.string)
    |> Json.Decode.Pipeline.required "len" (Json.Decode.int)
    |> Json.Decode.Pipeline.required "origin" (Json.Decode.string)

encodeSongItem : SongItem -> Json.Encode.Value
encodeSongItem record =
    Json.Encode.object
    [ ("origin",  Json.Encode.string <| record.origin)
    , ("pos",  Json.Encode.int <| record.pos)
    , ("name",  Json.Encode.string <| record.name)
    , ("len",  Json.Encode.int <| record.len)
    ]
