HPLAY
=====

# Hplay
Hplay is a MPD frontend intended for multiple users and scraps the web for contents.
Client written in Elm, Server written in Haskell.


Status: Alpha

# Screenshots
![Main menu](repoAssets/ss_main.png)
![Queue](repoAssets/ss_queue.png)

# Licence
GPLv3
