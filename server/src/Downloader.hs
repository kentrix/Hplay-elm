{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables, RecordWildCards #-}
module Downloader where

import Json
import Data.Text
import qualified Network.Download as DL
import System.Timeout
import qualified Data.ByteString.Lazy as BL
import qualified Data.ByteString.Internal as BS
import qualified Data.ByteString as BS
import System.FilePath.Posix ((</>), makeValid, addExtension, normalise)
import qualified Network.MPD as MPD
import qualified Data.String as S
import qualified Network.HTTP as HTTP
import qualified Network.URI as URI

data DownloadConfig = DownloadConfig { url :: Text, extension :: Text, songName :: Text, songSinger :: Text } deriving(Show)

data DownloadResult = DOk FilePath | DErr (Maybe String) deriving (Show)

musicPath = "/home/e/Music/"

clientDataToDownloadConfig :: ClientData -> Maybe DownloadConfig
clientDataToDownloadConfig cd = do
    a <- rURL cd
    b <- rExt cd
    c <- rName cd
    d <- rSingerName cd
    return $ DownloadConfig a b c d

downloadFile :: DownloadConfig -> IO (DownloadResult)
downloadFile cfg = do
    res <- timeout 60000000 . get . unpack $ url cfg
    putStrLn . show $ url cfg
    case res of
        Just r ->
            do
                let p' = unpack . replace "/" "-" $ songName cfg `append` "--" `append` songSinger cfg `append` "." `append` extension cfg
                {- TODO: More efficient string -}
                    p = normalise $ makeValid musicPath </> p'
                putStrLn p
                BS.writeFile p r
                return . DOk . makeValid $ p'
        _ -> do 
            putStrLn "Timeout"
            return $ DErr Nothing
    where
        get url = let uri = case URI.parseURI url of
                            Nothing -> error $ "Invalid URI: " ++ url
                            Just u -> u in
                HTTP.simpleHTTP (HTTP.defaultGETRequest_ uri) >>= HTTP.getResponseBody
